/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package we.session;

import java.util.List;
import web.utilities.Jurusan;

/**
 *
 * @author USER
 */
public interface IAuthenticationSessionJurusan {
    
    public List<Jurusan> getJurusanAll(String Dbname)throws Exception;
    public List<Jurusan> getJurusan(int id_jurusan, String Dbname)throws Exception;
    public void saveJurusan(Jurusan jurusan,String Dbname)throws Exception;
    public void updateJurusan(Jurusan jurusan,String Dbname)throws Exception;
    public void deleteJurusan(int id_jurusan,String Dbname)throws Exception;
}
