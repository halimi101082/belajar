/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package we.session;

import java.util.List;
import web.utilities.User;

/**
 *
 * @author oem
 */
public interface IAuthenticationSessionLogin {
    
    public List<User> getLogin(User user,String Dbname)throws Exception;
    public List<User> getUserAll(String Dbname)throws Exception;
    public List<User> getUser(int id_user,String Dbname)throws Exception;
    public void saveUser(User user,String Dbname)throws Exception;
    public void updateUser(User user,String Dbname)throws Exception;
    public void deleteUser(int id_user,String Dbname)throws Exception;
    
}
