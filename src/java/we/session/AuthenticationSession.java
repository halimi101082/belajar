/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package we.session;

import java.util.List;
import web.sql.AuthenticationSQL;
import web.sql.IAuthenticationSQL;
import web.utilities.Mahasiswa;


/**
 *
 * @author USER
 */
public class AuthenticationSession implements IAuthenticationSesion{

    @Override
    public List<Mahasiswa> getMappingMahasiswa(String dbserver) throws Exception {
        try{
            IAuthenticationSQL sql = new AuthenticationSQL();
            return sql.getMappingMahasiswa(dbserver);
        }catch(Exception ex){
            throw new Exception("AuthenticationSession : getMappingMahasiswa()\n"+ex.toString());
        }
    }

    @Override
    public List<Mahasiswa> getAllMahasiswa(int nim, String dbserver) throws Exception {
        try{
            IAuthenticationSQL sql = new AuthenticationSQL();
            return sql.getAllMahasiswa(nim,dbserver);
        }catch(Exception ex){
            throw new Exception("AuthenticationSession : getAdminName()\n"+ex.toString());
        }
    }

    @Override
    public void deleteMahasiswa(int nim, String dbserver) throws Exception {
        try {
            IAuthenticationSQL sql = new AuthenticationSQL();
            sql.deleteMahasiswa(nim, dbserver);
        } catch (Exception ex) {
            throw new Exception("AuthenticationSession : deleteMahasiswa\n" + ex.toString());
        }
    }

    @Override
    public void createMahasiswa(Mahasiswa obj, String dbserver) throws Exception {
        try{
            IAuthenticationSQL sql = new AuthenticationSQL();
            sql.createMahasiswa(obj, dbserver);
            
        }catch(Exception ex){
            throw new Exception("AuthenticationSession : createMahasiswa()\n"+ex.toString());
        }
    }

    @Override
    public void updateMahasiswa(Mahasiswa obj, String dbserver) throws Exception {
        try{
            IAuthenticationSQL sql = new AuthenticationSQL();
            sql.updateMahasiswa(obj, dbserver);
            
        }catch(Exception ex){
            throw new Exception("AuthenticationSession : updateMahasiswa()\n"+ex.toString());
        }
    }
    
}
