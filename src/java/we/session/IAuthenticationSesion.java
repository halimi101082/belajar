/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package we.session;

import java.util.List;
import web.utilities.Mahasiswa;

/**
 *
 * @author USER
 */
public interface IAuthenticationSesion {
    
    public List<Mahasiswa> getMappingMahasiswa(String dbserver) throws Exception;
    public List<Mahasiswa> getAllMahasiswa(int nim, String dbserver) throws Exception;
    public void deleteMahasiswa(int nim, String dbserver) throws Exception;
    public void createMahasiswa(Mahasiswa obj, String dbserver) throws Exception;
    public void updateMahasiswa(Mahasiswa obj, String dbserver) throws Exception;
     
}
