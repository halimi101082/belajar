/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package we.session;

import java.util.List;
import web.sql.AuthenticationJurusanSQL;
import web.sql.IAuthenticationJurusanSQL;
import web.utilities.Jurusan;

/**
 *
 * @author USER
 */
public class AuthenticationSessionJurusan implements IAuthenticationSessionJurusan{

    @Override
    public List<Jurusan> getJurusanAll(String Dbname) throws Exception {
        try{
            IAuthenticationJurusanSQL sql = new AuthenticationJurusanSQL();
            return sql.getJurusanAll(Dbname);
        }catch(Exception ex){
            throw new Exception("AuthenticationSession : getJurusan()\n"+ex.toString());
        }
    }

    @Override
    public List<Jurusan> getJurusan(int id_jurusan, String Dbname) throws Exception {
        try {
            IAuthenticationJurusanSQL sql=new AuthenticationJurusanSQL();
            return sql.cariJurusan(id_jurusan, Dbname);
            
        } catch (Exception ex) {
        throw new Exception(""+ex.getMessage());
            
        }
    }
    

    @Override
    public void saveJurusan(Jurusan jurusan, String Dbname) throws Exception {
        try {
            
        } catch (Exception ex) {
            throw new Exception("AuthenticationSession Error Save Jurusan \n"+ex.getMessage());
            
        }
    }

    @Override
    public void updateJurusan(Jurusan jurusan, String Dbname) throws Exception {
        try {
            
        } catch (Exception ex) {
        
            
        }
    }

    @Override
    public void deleteJurusan(int id_jurusan, String Dbname) throws Exception {
        try {
            
        } catch (Exception ex) {
        
            
        }
    }
    
}
