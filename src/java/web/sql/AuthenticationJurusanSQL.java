/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.sql;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import web.utilities.Jurusan;
import web.utilities.Mahasiswa;

/**
 *
 * @author USER
 */
public class AuthenticationJurusanSQL implements IAuthenticationJurusanSQL{
    
    private Connection getConnection(String dbServer)throws SQLException{
        Connection conn = null;
        try {
            ConnectionManager connMan = new ConnectionManager(dbServer);
             conn = connMan.getConnection();
             System.out.println("Koneksi Sukses");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AuthenticationJurusanSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AuthenticationJurusanSQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conn;
    }
    

    @Override
    public List<Jurusan> getJurusanAll(String dbserver) throws SQLException {
        Connection conn = getConnection(dbserver);
        String query = "SELECT * FROM jurusan";
        List<Jurusan> list = new ArrayList<>();
        Statement stm = null;
        ResultSet rs = null;
        
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(query);
            while (rs.next()) {
                
                Jurusan obj = new Jurusan();

                obj.setId(rs.getInt("id_jurusan"));
                obj.setJurusan(rs.getString("jurusan"));
                obj.setKeterangan(rs.getString("status"));

                list.add(obj);
            }
            
            return list;
            
        } catch (SQLException ex) {
            throw new SQLException("get Jurusan All : Gagal Ambil Jurusan\n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    @Override
    public List<Jurusan> cariJurusan(int id, String dbserver) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteJurusan(int id, String dbserver) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void createJurusan(Jurusan obj, String dbserver) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateJurusan(Jurusan obj, String dbserver) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
