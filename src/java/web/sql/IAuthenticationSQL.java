/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.sql;

import java.sql.SQLException;
import java.util.List;
import web.utilities.Mahasiswa;

/**
 *
 * @author USER
 */
public interface IAuthenticationSQL {
    
    public List<Mahasiswa> getMappingMahasiswa(String dbserver) throws SQLException;
    public List<Mahasiswa> getAllMahasiswa(int nim, String dbserver) throws SQLException;
    public void deleteMahasiswa(int nim, String dbserver) throws SQLException;
    public void createMahasiswa(Mahasiswa obj, String dbserver) throws SQLException;
    public void updateMahasiswa(Mahasiswa obj, String dbserver) throws SQLException;
}
