/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.sql;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import web.Mbean.Nasional;
import web.utilities.M_Desa;
import web.utilities.M_Kabupaten;
import web.utilities.M_Kecamatan;
import web.utilities.M_Propinsi;

/**
 *
 * @author acer
 */
public class AuthenticationRegionalSQL implements IAuthenticationRegionalSQL{
    
    private Connection getConnection(String dbServer)throws SQLException{
        Connection conn = null;
        try {
            ConnectionManager connMan = new ConnectionManager(dbServer);
             conn = connMan.getConnection();
             System.out.println("Koneksi Sukses");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AuthenticationJurusanSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AuthenticationJurusanSQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conn;
    }

    @Override
    public List<M_Propinsi> getAllPropinsi(String dbref) throws SQLException {
        
        Connection conn = getConnection(dbref);
        String query = "SELECT * FROM propinsi";
        List<M_Propinsi> list = new ArrayList<>();
        Statement stm = null;
        ResultSet rs = null;
        
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(query);
            while (rs.next()) {
                
                M_Propinsi obj = new M_Propinsi();

                obj.setId(rs.getString("id"));
                obj.setNama(rs.getString("nama"));

                list.add(obj);
            }
            
            return list;
            
        } catch (SQLException ex) {
            throw new SQLException("get Propinsi All : Gagal Ambil Propinsi\n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    @Override
    public List<M_Propinsi> getPropinsi(String index, String dbref) throws SQLException {
        Connection conn = getConnection(dbref);
        List<M_Propinsi> listProp=new ArrayList<>();
        String query = "select * from propinsi where id="+index+"";
        Statement st=null;
        ResultSet rs=null;
        try {
            st = conn.createStatement();
            rs=st.executeQuery(query);
            while (rs.next()) {                
                M_Propinsi propinsi = new M_Propinsi();
                propinsi.setId(rs.getString("id"));
                propinsi.setNama(rs.getString("nama"));
                
                listProp.add(propinsi);
            }
            return listProp;
        } catch (SQLException e) {
            throw new SQLException("get Jurusan All : Gagal Ambil Jurusan\n" + e.getMessage());
        }finally {
            if (st !=null) {
                st.close();
            }if (conn !=null) {
                conn.close();
            }
        }
        
    }

    @Override
    public List<M_Kabupaten> getAllKabupaten(String dbref) throws SQLException {
        Connection conn = getConnection(dbref);
        List<M_Kabupaten> listKab=new ArrayList<>();
        String query = "select * from kabupaten";
        Statement st=null;
        ResultSet rs=null;
        try {
            st = conn.createStatement();
            rs=st.executeQuery(query);
            while (rs.next()) {                
                M_Kabupaten kabupaten = new M_Kabupaten();
                kabupaten.setId(rs.getString("id"));
                kabupaten.setPropinsi_id(rs.getString("propinsi_id"));
                kabupaten.setNama(rs.getString("nama"));
                
                listKab.add(kabupaten);
            }
            return listKab;
        } catch (SQLException e) {
            throw new SQLException("get Kabupaten All : Gagal Ambil Kabupaten\n" + e.getMessage());
        }finally {
            if (st !=null) {
                st.close();
            }if (conn !=null) {
                conn.close();
            }
        }
    }

    @Override
    public List<M_Kabupaten> getKabupaten(String index, String dbref) throws SQLException {
        Connection conn = getConnection(dbref);
        List<M_Kabupaten> listKab=new ArrayList<>();
        String query = "select * from kabupaten where propinsi_id='"+index+"'";
        Statement st=null;
        ResultSet rs=null;
        try {
            st = conn.createStatement();
            rs=st.executeQuery(query);
            while (rs.next()) {                
                M_Kabupaten kabupaten = new M_Kabupaten();
                kabupaten.setId(rs.getString("id"));
                kabupaten.setPropinsi_id(rs.getString("propinsi_id"));
                kabupaten.setNama(rs.getString("nama"));
                
                listKab.add(kabupaten);
            }
            return listKab;
        } catch (SQLException e) {
            throw new SQLException("get Kabupaten All : Gagal Ambil Kabupaten\n" + e.getMessage());
        }finally {
            if (st !=null) {
                st.close();
            }if (conn !=null) {
                conn.close();
            }
        }
    }

    @Override
    public List<M_Kecamatan> getAllKecamatan(String dbref) throws SQLException {
        Connection conn = getConnection(dbref);
        List<M_Kecamatan> listKec=new ArrayList<>();
        String query = "select * from kecamatan";
        Statement st=null;
        ResultSet rs=null;
        try {
            st = conn.createStatement();
            rs=st.executeQuery(query);
            while (rs.next()) {                
                M_Kecamatan kecamatan = new M_Kecamatan();
                kecamatan.setId(rs.getString("id"));
                kecamatan.setKabupaten_id(rs.getString("kabupaten_id"));
                kecamatan.setNama(rs.getString("nama"));
                
                listKec.add(kecamatan);
            }
            return listKec;
        } catch (SQLException e) {
            throw new SQLException("get Kecamatan All : Gagal Ambil Kecamatan\n" + e.getMessage());
        }finally {
            if (st !=null) {
                st.close();
            }if (conn !=null) {
                conn.close();
            }
        }
    }

    @Override
    public List<M_Kecamatan> getKecamatan(String index, String dbref) throws SQLException {
        Connection conn = getConnection(dbref);
        List<M_Kecamatan> listKec=new ArrayList<>();
        String query = "select * from kecamatan where kabupaten_id='"+index+"'";
        Statement st=null;
        ResultSet rs=null;
        try {
            st = conn.createStatement();
            rs=st.executeQuery(query);
            while (rs.next()) {                
                M_Kecamatan kecamatan = new M_Kecamatan();
                kecamatan.setId(rs.getString("id"));
                kecamatan.setKabupaten_id(rs.getString("kabupaten_id"));
                kecamatan.setNama(rs.getString("nama"));
                
                listKec.add(kecamatan);
            }
            return listKec;
        } catch (SQLException e) {
            throw new SQLException("get Kecamatan All : Gagal Ambil Kecamatan\n" + e.getMessage());
        }finally {
            if (st !=null) {
                st.close();
            }if (conn !=null) {
                conn.close();
            }
        }
    }

    @Override
    public List<M_Desa> getAllDesa(String dbref) throws SQLException {
        Connection conn = getConnection(dbref);
        List<M_Desa> listDes=new ArrayList<>();
        String query = "select * from desa";
        Statement st=null;
        ResultSet rs=null;
        try {
            st = conn.createStatement();
            rs=st.executeQuery(query);
            while (rs.next()) {                
                M_Desa desa = new M_Desa();
                desa.setId(rs.getString("id"));
                desa.setKecamatan_id(rs.getString("kecamatan_id"));
                desa.setNama(rs.getString("nama"));
                
                listDes.add(desa);
            }
            return listDes;
        } catch (SQLException e) {
            throw new SQLException("get Kecamatan All : Gagal Ambil Kecamatan\n" + e.getMessage());
        }finally {
            if (st !=null) {
                st.close();
            }if (conn !=null) {
                conn.close();
            }
        }
    }

    @Override
    public List<M_Desa> getDesa(String index, String dbref) throws SQLException {
        Connection conn = getConnection(dbref);
        List<M_Desa> listDes=new ArrayList<>();
        String query = "select * from desa where kecamatan_id='"+index+"'";
        Statement st=null;
        ResultSet rs=null;
        try {
            st = conn.createStatement();
            rs=st.executeQuery(query);
            while (rs.next()) {                
                M_Desa desa = new M_Desa();
                desa.setId(rs.getString("id"));
                desa.setKecamatan_id(rs.getString("kecamatan_id"));
                desa.setNama(rs.getString("nama"));
                
                listDes.add(desa);
            }
            return listDes;
        } catch (SQLException e) {
            throw new SQLException("get Kecamatan All : Gagal Ambil Kecamatan\n" + e.getMessage());
        }finally {
            if (st !=null) {
                st.close();
            }if (conn !=null) {
                conn.close();
            }
        }
    }


    @Override
    public List<M_Kabupaten> getTreePropinsi(String dbref) throws SQLException {
        Connection conn = getConnection(dbref);
        List<M_Kabupaten> listPro=new ArrayList<>();
        String query = "select * from propinsi";
        Statement st=null;
        ResultSet rs=null;
        try {
            st = conn.createStatement();
            rs=st.executeQuery(query);
            while (rs.next()) {                
                M_Kabupaten provinsi = new M_Kabupaten();
                provinsi.setId(rs.getString("id"));
                provinsi.setNama(rs.getString("nama"));
                provinsi.setKet("propinsi");
                listPro.add(provinsi);
            }
            return listPro;
        } catch (SQLException e) {
            throw new SQLException("get Kabupaten All : Gagal Ambil Kabupaten\n" + e.getMessage());
        }finally {
            if (st !=null) {
                st.close();
            }if (conn !=null) {
                conn.close();
            }
        }
    }

    @Override
    public List<M_Kabupaten> getTreeKabupaten(String index, String dbref) throws SQLException {
        Connection conn = getConnection(dbref);
        List<M_Kabupaten> listKab=new ArrayList<>();
        String query = "select * from kabupaten where propinsi_id ='"+index+"'" ;
        Statement st=null;
        ResultSet rs=null;
        try {
            st = conn.createStatement();
            rs=st.executeQuery(query);
            while (rs.next()) {                
                M_Kabupaten kabupaten = new M_Kabupaten();
                kabupaten.setId(rs.getString("id"));
                kabupaten.setPropinsi_id(rs.getString("propinsi_id"));
                kabupaten.setNama(rs.getString("nama"));
                kabupaten.setKet("kabupaten");
                listKab.add(kabupaten);
            }
            return listKab;
        } catch (SQLException e) {
            throw new SQLException("get Kabupaten All : Gagal Ambil Kabupaten\n" + e.getMessage());
        }finally {
            if (st !=null) {
                st.close();
            }if (conn !=null) {
                conn.close();
            }
        }
    }

    @Override
    public List<M_Kabupaten> getTreeKecamatan(String index, String dbref) throws SQLException {
        Connection conn = getConnection(dbref);
        List<M_Kabupaten> listKec=new ArrayList<>();
        String query = "select * from kecamatan where kabupaten_id ='"+index+"'";
        Statement st=null;
        ResultSet rs=null;
        try {
            st = conn.createStatement();
            rs=st.executeQuery(query);
            while (rs.next()) {                
                M_Kabupaten kecamatan = new M_Kabupaten();
                kecamatan.setId(rs.getString("id"));
                kecamatan.setPropinsi_id(rs.getString("kabupaten_id"));
                kecamatan.setNama(rs.getString("nama"));
                kecamatan.setKet("kecamatan");
                listKec.add(kecamatan);
            }
            return listKec;
        } catch (SQLException e) {
            throw new SQLException("get Kecamatan All : Gagal Ambil Kecamatan\n" + e.getMessage());
        }finally {
            if (st !=null) {
                st.close();
            }if (conn !=null) {
                conn.close();
            }
        }
    }

    @Override
    public List<M_Kabupaten> getTreeDesa(String index, String dbref) throws SQLException {
        Connection conn = getConnection(dbref);
        List<M_Kabupaten> listDes=new ArrayList<>();
        String query = "select * from desa where kecamatan_id ='"+index+"'";
        Statement st=null;
        ResultSet rs=null;
        try {
            st = conn.createStatement();
            rs=st.executeQuery(query);
            while (rs.next()) {                
                M_Kabupaten desa = new M_Kabupaten();
                desa.setId(rs.getString("id"));
                desa.setPropinsi_id(rs.getString("kecamatan_id"));
                desa.setNama(rs.getString("nama"));
                desa.setKet("desa");
                listDes.add(desa);
            }
            return listDes;
        } catch (SQLException e) {
            throw new SQLException("get Kecamatan All : Gagal Ambil Kecamatan\n" + e.getMessage());
        }finally {
            if (st !=null) {
                st.close();
            }if (conn !=null) {
                conn.close();
            }
        }
    }

    @Override
    public List<Nasional> getNasional(int level,String id, String dbref) throws SQLException {
        Connection conn = getConnection(dbref);
        List<Nasional> listDes=new ArrayList<>();
        String table = "";
        String where = "";
        String query = "";
        switch(level){
            case 1: table = "kabupaten";
                    where = "where propinsi_id = '"+id+"'";
            break;
            case 2: table = "kecamatan";
                    where = "where kabupaten_id = '"+id+"'";
            break;
            case 3: table = "desa";
                    where = "where kecamatan_id = '"+id+"'";
            break;            
            default:
                table = "propinsi";
                where = "";
        }
        
        if (where !="") {
            query = "select * from "+table+" "+where+"";
        }else{
            query = "select * from "+table;
        }
        if(table =="desa"){
            query = "select id,nama from "+table+" "+where+"";
        }
        System.out.println("Query "+query);
        Statement st=null;
        ResultSet rs=null;
        try {
            st = conn.createStatement();
            rs=st.executeQuery(query);
            while (rs.next()) {                
                Nasional desa = new Nasional();
                desa.setId(rs.getString("id"));
                desa.setNama(rs.getString("nama"));
                desa.setLevel(level);
                listDes.add(desa);
            }
            return listDes;
        } catch (SQLException e) {
            throw new SQLException("get Kecamatan All : Gagal Semua\n" + e.getMessage());
        }finally {
            if (st !=null) {
                st.close();
            }if (conn !=null) {
                conn.close();
            }
        }
    }
}
