/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.sql;

import java.sql.SQLException;
import java.util.List;
import web.Mbean.Nasional;
import web.utilities.M_Desa;
import web.utilities.M_Propinsi;
import web.utilities.M_Kabupaten;
import web.utilities.M_Kecamatan;
import web.utilities.treeNasional;

/**
 *
 * @author acer
 */
public interface IAuthenticationRegionalSQL {
    public List<M_Propinsi> getAllPropinsi(String dbref)throws SQLException;
    public List<M_Kabupaten> getTreePropinsi(String dbref)throws SQLException;
    public List<M_Propinsi> getPropinsi(String index,String dbref)throws SQLException;
    public List<M_Kabupaten> getAllKabupaten(String dbref)throws SQLException;
    public List<M_Kabupaten> getTreeKabupaten(String index,String dbref)throws SQLException;
    public List<M_Kabupaten> getKabupaten(String index,String dbref)throws SQLException;
    public List<M_Kecamatan> getAllKecamatan(String dbref)throws SQLException;
    public List<M_Kabupaten> getTreeKecamatan(String index,String dbref)throws SQLException;
    public List<M_Kecamatan> getKecamatan(String index,String dbref)throws SQLException;
    public List<M_Desa> getAllDesa(String dbref)throws SQLException;
    public List<M_Kabupaten> getTreeDesa(String index,String dbref)throws SQLException;
    public List<M_Desa> getDesa(String index,String dbref)throws SQLException;    
    public List<Nasional> getNasional(int level,String id,String dbref)throws SQLException;    
}
