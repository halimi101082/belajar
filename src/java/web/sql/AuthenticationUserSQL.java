/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.sql;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import web.utilities.User;

/**
 *
 * @author acer
 */
public class AuthenticationUserSQL implements IAuthenticationUserSQL{
    
    private Connection getConnection(String dbserver)throws SQLException{
        Connection conn = null;
           
        try {
            ConnectionManager conman = new ConnectionManager(dbserver);
            conn = conman.getConnection();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AuthenticationUserSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AuthenticationUserSQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conn;
    }

    @Override
    public List<User> getAllUser(String dbserver) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<User> loginUser(String username, String password, String dbserver) throws SQLException {
        Connection conn=getConnection(dbserver);
        System.out.println("User dan password " + username + "password " + password);
        String SQL = "Select * FROM login WHERE username='"+username+"' and password='"+password+"'";
        Statement st = null;
        ResultSet rs = null;
        List<User> listUser = new ArrayList<>();
        System.out.println(""+SQL);
        try{
        st = conn.createStatement();
        rs = st.executeQuery(SQL);
        
        while (rs.next()) {
            User usr=new User();
            System.out.println(""+rs.getInt("id_user"));
            usr.setId(rs.getInt("id_user"));
            usr.setUsername(rs.getString("username"));
            usr.setPassword(rs.getString("password"));
            listUser.add(usr);
            
        }
        }catch (SQLException ex){
            throw new SQLException("Login User : Gagal Ambil Login User\n" + ex.getMessage());
        } finally {
            if (st != null) {
                st.close();
            }
            if(rs != null){
                rs.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        
        return listUser;
        
    }

    @Override
    public List<User> searchUser(long id, String dbserver) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteUser(int id, String dbserver) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void createUser(User obj, String dbserver) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateUser(User obj, String dbserver) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
