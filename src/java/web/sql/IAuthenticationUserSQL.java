/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.sql;

import java.sql.SQLException;
import java.util.List;
import web.utilities.User;

/**
 *
 * @author acer
 */
public interface IAuthenticationUserSQL{
    
    public List<User> getAllUser(String dbserver) throws SQLException;
    public List<User> loginUser(String username,String password, String dbserver) throws SQLException;
    public List<User> searchUser(long id, String dbserver) throws SQLException;
    public void deleteUser(int id, String dbserver) throws SQLException;
    public void createUser(User obj, String dbserver) throws SQLException;
    public void updateUser(User obj, String dbserver) throws SQLException;
    
}
