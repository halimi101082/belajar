/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.sql;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import web.utilities.Jurusan;
import web.utilities.Mahasiswa;

/**
 *
 * @author USER
 */
public class AuthenticationSQL implements IAuthenticationSQL{
    
    
    private Connection getConnection(String dbserver) throws SQLException {
        Connection conn = null;
        try {
            ConnectionManager connMan = new ConnectionManager(dbserver);
            conn = connMan.getConnection();
        } catch (ClassNotFoundException | SQLException | IOException ex) {
            throw new SQLException("CashAccountingSQLPerkadaLraCta : Gagal Koneksi.\n" + ex.toString());
        }
        return conn;
    }
    
    
    @Override
    public List<Mahasiswa> getMappingMahasiswa(String dbserver) throws SQLException {
        Connection conn = getConnection(dbserver);
        String query = "SELECT nim, nama, jk, id_jurusan, j.jurusan, j.status "
                + "FROM mahasiswa m "
                + "LEFT JOIN jurusan j ON m.jurusan = id_jurusan";
        Statement stm = null;
        ResultSet rs = null;
        List<Mahasiswa> list = new ArrayList<>();
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery(query);
            while (rs.next()) {
                Mahasiswa obj = new Mahasiswa();
                obj.setNim(rs.getInt("nim"));
                obj.setNama(rs.getString("nama"));
                obj.setJk(rs.getString("jk"));
                obj.setJurusan(new Jurusan(rs.getInt("id_jurusan"), rs.getString("jurusan"), ""));
                list.add(obj);
            }
            return list;
            
        } catch (SQLException ex) {
            throw new SQLException("CashAccountingSQLReportKonsolidasi : Gagal Ambil Mahasiswa\n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if(rs != null){
                rs.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    @Override
    public List<Mahasiswa> getAllMahasiswa(int nim, String dbserver) throws SQLException {
        System.out.println(".");
        Connection conn = getConnection(dbserver);
        Statement stm = null;
        List<Mahasiswa> acclist = new ArrayList<>();

        try {
            stm = conn.createStatement();

            String sql = "SELECT * FROM mahasiswa where nim = "+nim;
            
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Mahasiswa obj = new Mahasiswa();
                      obj.setNim(rs.getInt("nim"))  ;
                       obj.setNama(rs.getString("nama")); 
                       obj.setJk(rs.getString("jk")); 
//                       obj.setJurusan(rs.getInt("id_jurusan"));
                        
                
                acclist.add(obj);
            }

//            System.out.println("acclist: "+acclist.size());
            return acclist;
        } catch (SQLException ex) {
            throw new SQLException("MahasiswaSQL:: getAllMahasiswa\n" + ex.toString());
        } finally {
            if (stm != null) {
                stm.close();
            }

            conn.close();
        }
    }

    @Override
    public void deleteMahasiswa(int nim, String dbserver) throws SQLException {
       Connection conn = getConnection(dbserver);
        PreparedStatement pstm = null;

        try {
            
            String sql = "delete from mahasiswa where nim = " + nim;
            
            pstm = conn.prepareStatement(sql);
            pstm.execute();

        } catch (SQLException ex) {
            throw new SQLException("Mahasiswa:: deleteMahasiswa\n" + ex.toString());
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            conn.close();
        }
    }
    
    public boolean checkingNim(int nim, Connection conn) throws SQLException {
        Statement stm = null;
        boolean checking = false;

        try {
            stm = conn.createStatement();

            String sql = "SELECT nim FROM mahasiswa "
                    + " WHERE nim =" + nim;

            ResultSet rs = stm.executeQuery(sql);
            if (rs.next()) {
                checking = true;
            }

            return checking;
        } catch (SQLException ex) {
            throw new SQLException("MappingAkunSQL:: Checkin NIM Mahasiswa\n" + ex.toString());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public void createMahasiswa(Mahasiswa obj, String dbserver) throws SQLException {
        Connection conn = getConnection(dbserver);
        PreparedStatement pstm = null;
        int currentLevel = conn.getTransactionIsolation();
        conn.setAutoCommit(false);
        conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);

        try {
            boolean check=checkingNim(obj.getNim(), conn);
            String sql = "INSERT INTO mahasiswa(\n"
                    + "            nim, nama, jk, jurusan)\n"
                    + "    VALUES (?, ?, ?, ? );";
            pstm = conn.prepareStatement(sql);
            int cols = 1;
            pstm.setInt(cols++, obj.getNim());
            pstm.setString(cols++, obj.getNama());
            pstm.setString(cols++, obj.getJk());
            pstm.setInt(cols++, obj.getId_jurusan());
            
            if (!check) {
                
                pstm.executeUpdate();
            }
            
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
        } catch (SQLException ex) {
            conn.rollback();
            throw new SQLException("MahasiswaSQL:: CreateMahasiswa\n" + ex.toString());
        } finally {
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            if (pstm != null) {
                pstm.close();
            }

            conn.close();
        }
    }

    @Override
    public void updateMahasiswa(Mahasiswa obj, String dbserver) throws SQLException {
        Connection conn = getConnection(dbserver);
        PreparedStatement pstm = null;
        System.out.println("Kode Jurusannya saat Update "+ obj.getJurusan());
        try {
            boolean check = checkingNim(obj.getNim(), conn);
            
            String sql = "UPDATE mahasiswa\n"
                    + "   SET nama=?, jk=?, jurusan=? WHERE nim = " + obj.getNim();
            
            pstm = conn.prepareStatement(sql);
            int cols = 1;
            pstm.setString(cols++, obj.getNama());
            pstm.setString(cols++, obj.getJk());
            pstm.setInt(cols++, obj.getJurusan().getId());
            pstm.executeUpdate();
            if (!check) {
                throw new SQLException("NIM " + obj.getNim()+ " tidak sama.\n");
            } else {
                pstm.executeUpdate();
            }

        } catch (SQLException ex) {
            throw new SQLException("AuthenticationSession:: updateMahasiswa\n" + ex.toString());
        } finally {
            if (pstm != null) {
                pstm.close();
            }

            conn.close();
        }
    }
    
}
