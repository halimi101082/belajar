/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.sql;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Akira
 */
public class ConnectionManager {

    protected Connection m_conn = null;

    public ConnectionManager(String compname_dbname) throws ClassNotFoundException, SQLException, IOException {
        String driver = "org.postgresql.Driver";
        String user = "postgres";
        String password = "postgres";
        System.out.println("localhost:5432");
        Class.forName(driver);
        m_conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/" + compname_dbname, user, password);
    }

    public Connection getConnection() {
        return m_conn;
    }
}