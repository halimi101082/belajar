/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.sql;

import java.sql.SQLException;
import java.util.List;
import web.utilities.Jurusan;

/**
 *
 * @author USER
 */
public interface IAuthenticationJurusanSQL {
    
    public List<Jurusan> getJurusanAll(String dbserver) throws SQLException;
    public List<Jurusan> cariJurusan(int id, String dbserver) throws SQLException;
    public void deleteJurusan(int id, String dbserver) throws SQLException;
    public void createJurusan(Jurusan obj, String dbserver) throws SQLException;
    public void updateJurusan(Jurusan obj, String dbserver) throws SQLException; 
    
}
