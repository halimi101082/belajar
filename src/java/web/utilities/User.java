/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.utilities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author oem
 */
public class User {
    private int id_user;
    private String username;
    private String password;
    private boolean loging = true;
    private List<User> userList = new ArrayList<>();

    public User() {
    }

    public User(int id, String username, String password) {
        this.id_user = id_user;
        this.username = username;
        this.password = password;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String username) {
        this.username = username;
    }

    public User(int id) {
        this.id_user = id_user;
    }
    
    
    
    public int getId() {
        return id_user;
    }

    public void setId(int id) {
        this.id_user = id_user;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLoging() {
        return loging;
    }

    public void setLoging(boolean loging) {
        this.loging = loging;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
    
}
