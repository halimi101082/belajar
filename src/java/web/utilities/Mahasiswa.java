package web.utilities;

import java.io.Serializable;

/**
 *
 * @author akira
 */
public class Mahasiswa implements Serializable{
    private String  nama, jk;
    private int nim,id_jurusan;
    
    private Jurusan jurusan;

    public Mahasiswa() {
    }

    public Mahasiswa(String nama, String jk, int nim) {
        this.nama = nama;
        this.jk = jk;
        this.nim = nim;
    }

    public Mahasiswa(String nama, String jk, int nim, int id_jurusan) {
        this.nama = nama;
        this.jk = jk;
        this.nim = nim;
        this.id_jurusan = id_jurusan;
    }
    

    public Mahasiswa(String nama, String jk, int nim, Jurusan jurusan) {
        this.nama = nama;
        this.jk = jk;
        this.nim = nim;
        this.jurusan = jurusan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJk() {
        return jk;
    }

    public void setJk(String jk) {
        this.jk = jk;
    }

    public int getNim() {
        return nim;
    }

    public void setNim(int nim) {
        this.nim = nim;
    }

    public Jurusan getJurusan() {
        return jurusan;
    }

    public void setJurusan(Jurusan jurusan) {
        this.jurusan = jurusan;
    }
    
    public int getId_jurusan() {
        return id_jurusan;
    }

    public void setId_jurusan(int id_jurusan) {
        this.id_jurusan = id_jurusan;
    }

    @Override
    public String toString() {
        return nama;
    }

}
