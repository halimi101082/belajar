/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.utilities;

import java.io.Serializable;

/**
 *
 * @author USER
 */
public class Jurusan implements Serializable{
    private int id;
    private String jurusan,keterangan;
    private boolean isLoad;

    

    public Jurusan(int id, String jurusan, String keterangan) {
        this.id = id;
        this.jurusan = jurusan;
        this.keterangan = keterangan;
    }

    public Jurusan() {
        
    }
    
    

    public Jurusan(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
    public boolean isIsLoad() {
        return isLoad;
    }

    public void setIsLoad(boolean isLoad) {
        this.isLoad = isLoad;
    }

    @Override
    public String toString() {
        return jurusan;
    }
    
}
