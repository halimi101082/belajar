/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.utilities;

import java.io.Serializable;

/**
 *
 * @author acer
 */
public class M_Kecamatan implements Serializable{
    private String nama,id,kabupaten_id;
    private final String ket ="kecamatan";
    public M_Kecamatan() {
    }

    public M_Kecamatan(String nama, String id, String kabupaten_id) {
        this.nama = nama;
        this.id = id;
        this.kabupaten_id = kabupaten_id;
    }

    public M_Kecamatan(String nama, String id) {
        this.nama = nama;
        this.id = id;
    }

    public M_Kecamatan(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKabupaten_id() {
        return kabupaten_id;
    }

    public void setKabupaten_id(String kabupaten_id) {
        this.kabupaten_id = kabupaten_id;
    }

    public String getKet() {
        return ket;
    }

    

    @Override
    public String toString() {
        return nama;
    }
    
    
    
}
