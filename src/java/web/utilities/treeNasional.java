/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.utilities;

import java.io.Serializable;

/**
 *
 * @author acer
 */
public class treeNasional implements Serializable{
    private String propinsi,kabupaten,kecamatan,desa;

    public treeNasional() {
    }

    public treeNasional(String propinsi, String kabupaten, String kecamatan, String desa) {
        this.propinsi = propinsi;
        this.kabupaten = kabupaten;
        this.kecamatan = kecamatan;
        this.desa = desa;
    }

    public String getPropinsi() {
        return propinsi;
    }

    public void setPropinsi(String propinsi) {
        this.propinsi = propinsi;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getDesa() {
        return desa;
    }

    public void setDesa(String desa) {
        this.desa = desa;
    }

    @Override
    public String toString() {
        return propinsi;
    }

   
}
