/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.utilities;

import java.io.Serializable;

/**
 *
 * @author acer
 */
public class M_Desa implements Serializable{
    
    private String nama,id,kecamatan_id;
    private final String ket ="desa";
    public M_Desa() {
    }

    public M_Desa(String nama, String id, String kecamatan_id) {
        this.nama = nama;
        this.id = id;
        this.kecamatan_id = kecamatan_id;
    }

    public M_Desa(String nama, String id) {
        this.nama = nama;
        this.id = id;
    }

    public M_Desa(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKecamatan_id() {
        return kecamatan_id;
    }

    public void setKecamatan_id(String kecamatan_id) {
        this.kecamatan_id = kecamatan_id;
    }

    public String getKet() {
        return ket;
    }
    
    
    
    @Override
    public String toString() {
        return nama;
    }
    
    
    
}
