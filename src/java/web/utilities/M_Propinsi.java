/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.utilities;

import java.io.Serializable;

/**
 *
 * @author acer
 */
public class M_Propinsi implements Serializable{
    private String id,nama;
    private final String ket= "provinsi";

    public M_Propinsi() {
    }
    
    public M_Propinsi(String id, String nama) {
        this.id = id;
        this.nama = nama;
    }
    
    
    
    public M_Propinsi(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    
    @Override
    public String toString() {
        return nama;
    }

    public String getKet() {
        return ket;
    }
    
    
}
