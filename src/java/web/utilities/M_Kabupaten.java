/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.utilities;

import java.io.Serializable;

/**
 *
 * @author acer
 */
public class M_Kabupaten implements Serializable{
    private String nama,id,propinsi_id,ket;

    public M_Kabupaten() {
    }

    public M_Kabupaten(String nama, String id, String propinsi_id) {
        this.nama = nama;
        this.id = id;
        this.propinsi_id = propinsi_id;
    }

    public M_Kabupaten(String nama, String propinsi_id) {
        this.nama = nama;
        this.propinsi_id = propinsi_id;
    }

    public M_Kabupaten(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPropinsi_id() {
        return propinsi_id;
    }

    public void setPropinsi_id(String propinsi_id) {
        this.propinsi_id = propinsi_id;
    }
    
    
    @Override
    public String toString() {
        return nama;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }
    
    
}
