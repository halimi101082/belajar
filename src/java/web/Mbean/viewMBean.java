/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.Mbean;
 
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
 
import org.primefaces.event.CloseEvent;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;
import web.sql.AuthenticationJurusanSQL;
import web.sql.AuthenticationRegionalSQL;
import web.sql.AuthenticationSQL;
import web.sql.IAuthenticationJurusanSQL;
import web.sql.IAuthenticationRegionalSQL;
import web.sql.IAuthenticationSQL;
import web.utilities.Jurusan;
import web.utilities.M_Desa;
import web.utilities.M_Kabupaten;
import web.utilities.M_Kecamatan;
import web.utilities.M_Propinsi;
import web.utilities.Mahasiswa;
 
@ManagedBean(name="viewMBean")
@ViewScoped
public class viewMBean implements Serializable{
    private List<Mahasiswa> listMhs =new ArrayList<>();
    private List<Jurusan> listJurusan = new ArrayList<>();
    private TreeNode root,root1, selectedNode,selectedNode1;
    private UploadedFile file;
    private Mahasiswa mahasiswa = new Mahasiswa();
    private List<Mahasiswa> liistm = new ArrayList<>();
    private List<M_Kabupaten> listPropinsi = new ArrayList<>();
    private List<Nasional> listNasional = new ArrayList<>();
    private List<M_Kabupaten> listKabupaten = new ArrayList<>();
    private List<M_Kecamatan> listKecamatan = new ArrayList<>();
    private List<M_Desa> listDesa = new ArrayList<>();
    private List<Mahasiswa> liist = new ArrayList<>();
    DefaultTreeNode prof;
    DefaultTreeNode kab;
    DefaultTreeNode kec;
    DefaultTreeNode des;
    IAuthenticationRegionalSQL authre= new AuthenticationRegionalSQL();
    public viewMBean() {
    }
    
    @PostConstruct
    void init(){
        try {
            
            IAuthenticationSQL mhs = new AuthenticationSQL();
            IAuthenticationJurusanSQL jrs= new AuthenticationJurusanSQL();
            listNasional = authre.getNasional(0,null,"mahasiswa");
            
            root1 = new DefaultTreeNode("");
            
            listNasional.stream().forEach((ppp)-> {
                new DefaultTreeNode(ppp,root1);
                
            }); 
        
            listJurusan = jrs.getJurusanAll("mahasiswa");
            listMhs = mhs.getMappingMahasiswa("mahasiswa");
            root = new DefaultTreeNode(listMhs);
            
            listJurusan.stream().forEach((abc)-> {
                new DefaultTreeNode(abc,root);
            });       
        } catch (SQLException ex) {
            Logger.getLogger(viewMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    public void onNodeExpand(NodeExpandEvent event){
        TreeNode node = event.getTreeNode();
        Jurusan jrs = (Jurusan) node.getData();
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Expanded", event.getTreeNode().toString());
        FacesContext.getCurrentInstance().addMessage(null, message);
        if(!jrs.isIsLoad()){
            for(Mahasiswa obj : listMhs){
                if(obj.getJurusan().toString().equals(jrs.getJurusan())){
                    new DefaultTreeNode(obj, node);
                }
            }
            
            jrs.setIsLoad(true);
        
            
        }
    }
    
    public void onNodeSelect(NodeSelectEvent event) {
        liistm.clear();
        TreeNode node = event.getTreeNode();
        Jurusan jrs = (Jurusan) node.getData();
        
         if(!jrs.isIsLoad()){
            for(Mahasiswa obj : listMhs){
                if(obj.getJurusan().toString().equals(jrs.getJurusan())){
                    new DefaultTreeNode(obj, node);
                }
            }
            
            jrs.setIsLoad(true);
        }
         
        for(Mahasiswa obj : listMhs){
                if(obj.getJurusan().toString().equals(jrs.getJurusan())){
                   liistm.add(obj);
                }
            }
        FacesMessage growl = new FacesMessage(FacesMessage.SEVERITY_INFO, "Selected", event.getTreeNode().getData().toString());
        FacesContext.getCurrentInstance().addMessage(null, growl);
    }
 
    
    public void onNodeCollapse(NodeCollapseEvent event){
        event.getTreeNode().setExpanded(false);
    }
    
    public void upload() {
        if(file != null) {
            FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            System.out.println(""+file);
        }
    }
    
    
    public void onClose(CloseEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Panel Closed", "Closed panel id:'" + event.getComponent().getId() + "'");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
     
    public void onToggle(ToggleEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, event.getComponent().getId() + " toggled", "Status:" + event.getVisibility().name());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public void back(){
        try {
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            externalContext.redirect("mahasiswa.jsf");
        } catch (IOException ex) {
            Logger.getLogger(viewMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onNodeExpand1(NodeExpandEvent event) {
        System.out.println("Masuk Seleect Node");
            
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Expanded", event.getTreeNode().toString());
            FacesContext.getCurrentInstance().addMessage(null, message);
        
    }
 
    public void onNodeCollapse1(NodeCollapseEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Collapsed", event.getTreeNode().toString());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
 
    public void onNodeSelect1(NodeSelectEvent event) {
 
        try {
            TreeNode node = event.getTreeNode();
            Nasional pro = (Nasional) node.getData();
            System.out.println(" ID Province " +pro.getLevel());
            listNasional = authre.getNasional(pro.getLevel()+1,pro.getId(), "mahasiswa");
            if (pro.getLevel() <3) {
                for (Nasional nas : listNasional) {
                kab = new DefaultTreeNode(nas, node);
            }
            }
            
//            if (pro.getKet().equals("propinsi")) {
//                listKabupaten = authre.getTreeKabupaten(pro.getId(), "mahasiswa");
//                for (M_Kabupaten lkab : listKabupaten) {
//                    kab = new DefaultTreeNode(lkab, node);
//                }
//            }else if(pro.getKet().equals("kabupaten")){
//                System.out.println("Get tree Kecamatan" +pro.getKet());
//                listKabupaten = authre.getTreeKecamatan(pro.getId(), "mahasiswa");
//                for (M_Kabupaten lkab : listKabupaten) {
//                    System.out.println("Kemacatan");
//                    kec = new DefaultTreeNode(lkab, node);
//                }
//            }else if(pro.getKet().equals("kecamatan")){
//                listKabupaten = authre.getTreeDesa(pro.getId(), "mahasiswa");
//                for (M_Kabupaten lkab : listKabupaten) {
//                    System.out.println("Desooo");
//                    des = new DefaultTreeNode(lkab, node);
//                }
//            
//            }

        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Selected", event.getTreeNode().toString());
        FacesContext.getCurrentInstance().addMessage(null, message);
        } catch (SQLException ex) {
            Logger.getLogger(viewMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
 
    public void onNodeUnselect(NodeUnselectEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Unselected", event.getTreeNode().toString());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public Mahasiswa getMahasiswa() {
        return mahasiswa;
    }

    public void setMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
    }
    
    

    public List<Mahasiswa> getLiistm() {
        return liistm;
    }

    public void setLiistm(List<Mahasiswa> liistm) {
        this.liistm = liistm;
    }
    
    
    public List<Mahasiswa> getListMhs() {
        return listMhs;
    }

    public void setListMhs(List<Mahasiswa> listMhs) {
        this.listMhs = listMhs;
    }

    public List<Jurusan> getListJurusan() {
        return listJurusan;
    }

    public void setListJurusan(List<Jurusan> listJurusan) {
        this.listJurusan = listJurusan;
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public TreeNode getRoot1() {
        return root1;
    }

    public void setRoot1(TreeNode root1) {
        this.root1 = root1;
    }

    public TreeNode getSelectedNode1() {
        return selectedNode1;
    }

    public void setSelectedNode1(TreeNode selectedNode1) {
        this.selectedNode1 = selectedNode1;
    }

    public List<M_Kabupaten> getListPropinsi() {
        return listPropinsi;
    }

    public void setListPropinsi(List<M_Kabupaten> listPropinsi) {
        this.listPropinsi = listPropinsi;
    }

    public List<Nasional> getListNasional() {
        return listNasional;
    }

    public void setListNasional(List<Nasional> listNasional) {
        this.listNasional = listNasional;
    }

    public List<M_Kabupaten> getListKabupaten() {
        return listKabupaten;
    }

    public void setListKabupaten(List<M_Kabupaten> listKabupaten) {
        this.listKabupaten = listKabupaten;
    }

    public List<M_Kecamatan> getListKecamatan() {
        return listKecamatan;
    }

    public void setListKecamatan(List<M_Kecamatan> listKecamatan) {
        this.listKecamatan = listKecamatan;
    }

    public List<M_Desa> getListDesa() {
        return listDesa;
    }

    public void setListDesa(List<M_Desa> listDesa) {
        this.listDesa = listDesa;
    }

    public List<Mahasiswa> getLiist() {
        return liist;
    }

    public void setLiist(List<Mahasiswa> liist) {
        this.liist = liist;
    }
    
    
    
    
}