/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.Mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import we.session.AuthenticationSessionJurusan;
import web.sql.AuthenticationJurusanSQL;
import web.sql.IAuthenticationJurusanSQL;
import web.utilities.Jurusan;

/**
 *
 * @author USER
 */
@ManagedBean(name="jurusanMBean")
@ViewScoped
public class JurusanMBean implements Serializable{
    private int id;
    private String jurusan,keteragan;
    private List<Jurusan> listJurusan=new ArrayList<>();
    
    @PostConstruct
    void init(){
        try {
            IAuthenticationJurusanSQL auth = new AuthenticationJurusanSQL();
            listJurusan = auth.getJurusanAll("mahasiswa");
            System.out.println("Masuk Jurusan get All " +listJurusan.toString());
        } catch (Exception ex) {
            Logger.getLogger(JurusanMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void Simpan(){
        
    }
    public void Update(){
        
    }
    public void Delete(){
        
    }

    public JurusanMBean(int id) {
        this.id = id;
    }

    public JurusanMBean(List<Jurusan> listJurusan) {
        this.listJurusan = listJurusan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public String getKeteragan() {
        return keteragan;
    }

    public void setKeteragan(String keteragan) {
        this.keteragan = keteragan;
    }

    public List<Jurusan> getListJurusan() {
        return listJurusan;
    }

    public void setListJurusan(List<Jurusan> listJurusan) {
        this.listJurusan = listJurusan;
    }

}
