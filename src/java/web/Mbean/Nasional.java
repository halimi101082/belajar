/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.Mbean;

/**
 *
 * @author acer
 */
public class Nasional {
    private int level;
    private String id,nama;

    public Nasional() {
    }

    public Nasional(int level, String id, String nama) {
        this.level = level;
        this.id = id;
        this.nama = nama;
    }

    public Nasional(int level) {
        this.level = level;
    }

    public Nasional(String nama) {
        this.nama = nama;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    @Override
    public String toString() {
        return nama;
    }
    
    
    
    
    
}
