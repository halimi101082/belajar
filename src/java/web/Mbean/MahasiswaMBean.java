package web.Mbean;

import web.utilities.Mahasiswa;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import web.sql.AuthenticationJurusanSQL;
import web.sql.AuthenticationRegionalSQL;
import web.sql.AuthenticationSQL;
import web.sql.IAuthenticationJurusanSQL;
import web.sql.IAuthenticationRegionalSQL;
import web.sql.IAuthenticationSQL;
import web.utilities.Jurusan;
import web.utilities.M_Desa;
import web.utilities.M_Kabupaten;
import web.utilities.M_Kecamatan;
import web.utilities.M_Propinsi;
import web.utilities.treeNasional;


/**
 *
 * @author akira
 */
@ManagedBean(name = "mahasiswaMBean")
@ViewScoped
public class MahasiswaMBean implements Serializable{
    private String  nama,jk,id,propinsi_id,kabupaten_id,kecamatan_id,desa_id;
    private int nim;
    private List<M_Propinsi> listPropinsi = new ArrayList<>();
    private List<Nasional> listNasional = new ArrayList<>();
    private List<M_Kabupaten> listKabupaten = new ArrayList<>();
    private List<M_Kecamatan> listKecamatan = new ArrayList<>();
    private List<M_Desa> listDesa = new ArrayList<>();
    private M_Propinsi listOnchange;
    private Nasional nasional;
    private M_Propinsi propinsi;
    private M_Kabupaten kabupaten;
    private M_Kecamatan kecamatan;
    private M_Desa desa;
    private List<Mahasiswa> listMhs=new ArrayList<>();
    private Mahasiswa selectedMhs;
    private TreeNode root;
    private int jurusan;
    private boolean rendered_kab,rendered_kec,rendered_desa;
    private List<Jurusan> listJurusan=new ArrayList<>();
    
    
    @PostConstruct
    void init(){
        try {
            IAuthenticationSQL auth = new AuthenticationSQL();
            IAuthenticationJurusanSQL auth2 = new AuthenticationJurusanSQL();
            IAuthenticationRegionalSQL authR = new AuthenticationRegionalSQL();
            listPropinsi = authR.getAllPropinsi("mahasiswa");
            listMhs = auth.getMappingMahasiswa("mahasiswa");
            listJurusan = auth2.getJurusanAll("mahasiswa");

        } catch (Exception ex) {
            Logger.getLogger(MahasiswaMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void getNode(){
        try {
            IAuthenticationRegionalSQL authR = new AuthenticationRegionalSQL();
            listNasional = authR.getNasional(0,null,"mahasiswa");
            root = new DefaultTreeNode("Root",null);
            System.out.println("List Propinsi coooyyy   "+root);
            for (Nasional ns : listNasional) {
                new DefaultTreeNode(ns,root);
            }      
        } catch (Exception e) {
        }
    }
    
    
    
    public TreeNode getRoot() {    
        return root;
    }
    
    
    public void selectPropinsi(){
            System.out.println("Select Propinsi " + propinsi_id);
        try {
            IAuthenticationRegionalSQL authR = new AuthenticationRegionalSQL();
            listKabupaten = authR.getKabupaten(propinsi_id,"mahasiswa");
            rendered_kab = true;
            listKecamatan = null;
            listDesa = null;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public void selectKabupaten(){
            System.out.println("Select Kabupaten "+kabupaten_id);
         try {
           
                IAuthenticationRegionalSQL authR = new AuthenticationRegionalSQL();
                listKecamatan = authR.getKecamatan(kabupaten_id,"mahasiswa");
                rendered_kec=true;
                listDesa = null;
                         
        } catch (Exception ex) {
            Logger.getLogger(MahasiswaMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void selectKecamatan(){
        System.out.println("Select kecamatan "+kecamatan_id);
         try {
            
                IAuthenticationRegionalSQL authR = new AuthenticationRegionalSQL();
                listDesa = authR.getDesa(kecamatan_id, "mahasiswa");
                rendered_desa=true;
             
            
        } catch (Exception ex) {
            Logger.getLogger(MahasiswaMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void ambilData(){
        System.out.println(" ID Propinsi " + propinsi_id);
        System.out.println(" ID Kabupaten " + kabupaten_id);
        System.out.println(" ID Kecamatan " + kecamatan_id);
    }

   
    
    public void onSelectedMhs(){
        if(selectedMhs!=null){
            nim = selectedMhs.getNim();
            nama = selectedMhs.getNama();
            jk = selectedMhs.getJk();
            jurusan = selectedMhs.getJurusan().getId();
        }
    }
    
    public void simpanMhs(){
        
        if (nim != 0 || nama.isEmpty()==false || jurusan != 0) {           
            try {
//                 System.out.println("ana tah beli kinih "+jurusan);
                Mahasiswa mhs = new Mahasiswa(nama, jk, nim,jurusan);
                IAuthenticationSQL auth = new AuthenticationSQL();
                auth.createMahasiswa(mhs, "mahasiswa");
                
//                listMhs.add(new Mahasiswa(nama, jk, nim, jurusan));
                init();
                clear();
            } catch (Exception ex) {
                Logger.getLogger(MahasiswaMBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }else{
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN,
                                "Perhatian", "NIM Nama dan Jurusan tidak boleh kosong"));
             
        }
        
    }
    
    public void submit() {
        try {
            //Does stuff
//            System.out.println(""+selectedMhs.getNim());
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            externalContext.redirect("view.jsf");
        } catch (IOException ex) {
            Logger.getLogger(MahasiswaMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    
}
 
    public void editMhs(){
       Mahasiswa mhs = new Mahasiswa(nama, jk, nim, new Jurusan(jurusan));
        if(mhs!=null){
            try {
                IAuthenticationSQL auth = new AuthenticationSQL();
                auth.updateMahasiswa(mhs, "mahasiswa");
                init();
                clear();
            } catch (Exception ex) {
                Logger.getLogger(MahasiswaMBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, 
                    new FacesMessage(FacesMessage.SEVERITY_WARN, 
                            "Perhatian", "Silahkan Pilih Mahasiswa Yang Akan Di Edit"));
        }
    }
    
    public void hapusMhs(){
        
        if(selectedMhs!=null){
            try {
                IAuthenticationSQL auth=new AuthenticationSQL();
                auth.deleteMahasiswa(nim, "mahasiswa");
                listMhs.remove(selectedMhs);
                clear();
                addMessage("sukses", "Data" + nama + " berhasil dihapus");
            } catch (Exception ex) {
                Logger.getLogger(MahasiswaMBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, 
                    new FacesMessage(FacesMessage.SEVERITY_WARN, 
                            "Perhatian", "Silahkan Pilih Mahasiswa"));
        }
    }
    
    public void onNodeExpand(NodeSelectEvent event){
        TreeNode node = event.getTreeNode();
        Jurusan jrs = (Jurusan) node.getData();
        
        if(!jrs.isIsLoad()){
            for(Mahasiswa obj : listMhs){
                if(obj.getJurusan().equals(jrs.getJurusan())){
                    new DefaultTreeNode(obj, node);
                }
            }
            jrs.setIsLoad(true);
        }
    }
    
    public void onNodeCollapse(NodeCollapseEvent event){
        event.getTreeNode().setExpanded(false);
    }
    
    public void onSelectedMahasiswa(){
        
    }

    private void clear(){
        nim = 0;
        nama = "";
        jk = "";
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJk() {
        return jk;
    }

    public void setJk(String jk) {
        this.jk = jk;
    }

    public int getNim() {
        return nim;
    }

    public void setNim(int nim) {
        this.nim = nim;
    }

    public List<Mahasiswa> getListMhs() {
        return listMhs;
    }

    public void setListMhs(List<Mahasiswa> listMhs) {
        this.listMhs = listMhs;
    }

    public Mahasiswa getSelectedMhs() {
        return selectedMhs;
    }

    public void setSelectedMhs(Mahasiswa selectedMhs) {
        this.selectedMhs = selectedMhs;
    }

    public int getJurusan() {
        return jurusan;
    }

    public void setJurusan(int jurusan) {
        this.jurusan = jurusan;
    }

    public List<Jurusan> getListJurusan() {
        return listJurusan;
    }

    public void setListJurusan(List<Jurusan> listJurusan) {
        this.listJurusan = listJurusan;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPropinsi_id() {
        return propinsi_id;
    }

    public void setPropinsi_id(String propinsi_id) {
        this.propinsi_id = propinsi_id;
    }

    public String getKabupaten_id() {
        return kabupaten_id;
    }

    public void setKabupaten_id(String kabupaten_id) {
        this.kabupaten_id = kabupaten_id;
    }

    public String getKecamatan_id() {
        return kecamatan_id;
    }

    public void setKecamatan_id(String kecamatan_id) {
        this.kecamatan_id = kecamatan_id;
    }

    public List<M_Propinsi> getListPropinsi() {
        return listPropinsi;
    }

    public void setListPropinsi(List<M_Propinsi> listPropinsi) {
        this.listPropinsi = listPropinsi;
    }

    public List<M_Kabupaten> getListKabupaten() {
        return listKabupaten;
    }

    public void setListKabupaten(List<M_Kabupaten> listKabupaten) {
        this.listKabupaten = listKabupaten;
    }

    public List<M_Kecamatan> getListKecamatan() {
        return listKecamatan;
    }

    public void setListKecamatan(List<M_Kecamatan> listKecamatan) {
        this.listKecamatan = listKecamatan;
    }

    public List<M_Desa> getListDesa() {
        return listDesa;
    }

    public void setListDesa(List<M_Desa> listDesa) {
        this.listDesa = listDesa;
    }

    public M_Propinsi getPropinsi() {
        return propinsi;
    }

    public void setPropinsi(M_Propinsi propinsi) {
        this.propinsi = propinsi;
    }

    public M_Kabupaten getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(M_Kabupaten kabupaten) {
        this.kabupaten = kabupaten;
    }

    public M_Kecamatan getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(M_Kecamatan kecamatan) {
        this.kecamatan = kecamatan;
    }

    public M_Desa getDesa() {
        return desa;
    }

    public void setDesa(M_Desa desa) {
        this.desa = desa;
    }

    public M_Propinsi getListOnchange() {
        return listOnchange;
    }

    public void setListOnchange(M_Propinsi listOnchange) {
        this.listOnchange = listOnchange;
    }

    public boolean isRendered_kab() {
        return rendered_kab;
    }

    public void setRendered_kab(boolean rendered_kab) {
        this.rendered_kab = rendered_kab;
    }

    public boolean isRendered_kec() {
        return rendered_kec;
    }

    public void setRendered_kec(boolean rendered_kec) {
        this.rendered_kec = rendered_kec;
    }

    public boolean isRendered_desa() {
        return rendered_desa;
    }

    public void setRendered_desa(boolean rendered_desa) {
        this.rendered_desa = rendered_desa;
    }

    public String getDesa_id() {
        return desa_id;
    }

    public void setDesa_id(String desa_id) {
        this.desa_id = desa_id;
    }
    
    
    
    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
}
