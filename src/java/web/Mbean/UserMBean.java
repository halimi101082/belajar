/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.Mbean;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import web.sql.AuthenticationUserSQL;
import web.sql.IAuthenticationUserSQL;
import web.utilities.User;

/**
 *
 * @author oem
 */
@ManagedBean(name="userMBean")
@ViewScoped
public class UserMBean  implements Serializable{
    private int id_user;
    private String user,password,firstname,lastname,email;
    private List<User> liistUser;
    private IAuthenticationUserSQL userService;
    public UserMBean() {
    }
    
    @PostConstruct
    void init(){
        liistUser=new ArrayList<>();
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserMBean{" + "user=" + user + '}';
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<User> getLiistUser() {
        return liistUser;
    }

    public void setLiistUser(List<User> liistUser) {
        this.liistUser = liistUser;
    }

    public IAuthenticationUserSQL getUserService() {
        return userService;
    }

    public void setUserService(IAuthenticationUserSQL userService) {
        this.userService = userService;
    }
    
    
    public void testInternet(){
        boolean connectivity;
        try 
        {
            URL url = new URL("http://www.google.com");

            URLConnection connection = url.openConnection();
            connection.connect();   

            System.out.println("Internet Connected");   
            connectivity = true;
         }catch (Exception e){

            System.out.println("Sorry, No Internet Connection");     
            connectivity = false;
        } 
    }
    
    public void klikLogin() throws SQLException{
        
     try 
        {  
         
          userService =new AuthenticationUserSQL();
          liistUser = userService.loginUser(user, password, "mahasiswa");
         if (!liistUser.isEmpty())
            {
                if (liistUser.get(0).getUsername().equals(user) && liistUser.get(0).getPassword().equals(password)) {
                   ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                   externalContext.redirect("mahasiswa.jsf");
                }
           }else{
             FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage("Username atau password salah !!!"));
         }
        } catch (IOException ex) {
            Logger.getLogger(MahasiswaMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
       
}
